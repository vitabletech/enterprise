//jQuery for page scrolling feature - requires jQuery Easing plugin
$(document).ready(function() {
  $(function() {
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1200, 'easeInOutExpo');
        event.preventDefault();
    });
});
if($('.TabsMainSection').length > 0 ){
    $(".TabsMainSection").each(function(){
      $('.TabsMainSection .TabList li:first-child').addClass('current');
      $('.TabsMainSection .TabContent:first-child').addClass('active');
      $('.TabsMainSection .TabList li').on('click',function(e){
        e.preventDefault();
        var currLink = $(this);
        $(this).addClass('current').siblings("li").removeClass('current');
        var navi = $(this).attr('data-name');
        $(this).closest('.TabsMainSection').find('.TabContent').removeClass('active');
        $(this).closest('.TabsMainSection').find('div.'+navi).addClass('active');
      });
    });
}

let tabId1 = $('#tab1').attr('id');
$('#content' + tabId1.substring(3)).show();

let tabId2 = $('#tab7').attr('id');
$('#content' + tabId2.substring(3)).show();

let tabId3 = $('#tab13').attr('id');
$('#content' + tabId3.substring(3)).show();


// Listen for changes in the radio buttons
$('input[name="tabs"]').change(function() {
  // Hide all tab content
  $('.tab-content').hide();
  // Show the content for all checked tabs
  $('input[name="tabs"]:checked').each(function() {
      var tabId = $(this).attr('id');
      $('#content' + tabId.substring(3)).show();
  });
}); 
});